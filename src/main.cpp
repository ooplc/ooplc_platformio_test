#include <Arduino.h>

// put function declarations here:
int myFunction(int, int);

void setup() {
  // put your setup code here, to run once:
  int result = myFunction(2, 3);
  pinMode(PB_2, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(PB_2, HIGH);
  delay(500);
  digitalWrite(PB_2, LOW);
  delay(250);
}

// put function definitions here:
int myFunction(int x, int y) {
  return x + y;
}